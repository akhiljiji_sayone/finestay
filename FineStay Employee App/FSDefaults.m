//
//  FSDefaults.m
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import "FSDefaults.h"
#import "FSDebugLogger.h"

@interface FSDefaultsFile : NSObject

+ (FSDefaultsFile *)sharedInstance;

@property (nonatomic, strong)NSString *backingFileName;
@property (nonatomic, strong)NSMutableDictionary *defaultDictionary;

- (id)objectForKey:(NSString *)key;
- (BOOL)setObject:(id)obj forKey:(NSString *)key;
- (BOOL)removeObjectForKey:(NSString *)key;

@end

@implementation FSDefaultsFile
+(FSDefaultsFile *)sharedInstance
{
    static FSDefaultsFile *__sharedBEDefaultsFile = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedBEDefaultsFile = [[FSDefaultsFile alloc] init];
    });
    
    return __sharedBEDefaultsFile;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self _refreshBackingFile];
    }
    
    return self;
}

- (void)_refreshBackingFile
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *libraryDirectory = [paths objectAtIndex:0];
        
        self.backingFileName = [NSString stringWithFormat:@"%@/FSdefaults.plist",libraryDirectory];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:_backingFileName]) {
            // We need to create the dictionary file
            if([[NSDictionary dictionary] writeToFile:_backingFileName atomically:YES]) {
                NSError *error = nil;
                // Change the protection class of this FScause we need this in the background
                [fileManager setAttributes:[NSDictionary dictionaryWithObject:@"NSFileProtectionNone" forKey:@"NSFileProtectionKey"] ofItemAtPath:_backingFileName error:&error];
            }
        }
    });
    
    self.defaultDictionary = [[NSDictionary dictionaryWithContentsOfFile:_backingFileName] mutableCopy];
}

- (id)objectForKey:(NSString *)key
{
    return [_defaultDictionary objectForKey:key];
}

- (BOOL)setObject:(id)obj forKey:(NSString *)key
{
    if (key.length == 0 || !obj)
        return NO;
    
    if ([obj isEqual:[_defaultDictionary objectForKey:key]])
        return YES;
    
    [_defaultDictionary setObject:obj forKey:key];
    
    BOOL success;
    NS_DURING
    success = [[_defaultDictionary copy] writeToFile:_backingFileName atomically:YES];
    NS_HANDLER
    NS_ENDHANDLER
    
    return success;
}

- (BOOL)removeObjectForKey:(NSString *)key
{
    if(![_defaultDictionary objectForKey:key])
        return YES;
    
    [_defaultDictionary removeObjectForKey:key];
    NS_DURING
    return [[_defaultDictionary copy] writeToFile:_backingFileName atomically:YES];
    NS_HANDLER
    return false;
    NS_ENDHANDLER
}

@end

NS_INLINE id _defaultForKey(NSString *key) {
    return [[FSDefaultsFile sharedInstance] objectForKey:key];
}

id FSGetDefault(NSString *key)
{
    return _defaultForKey(key);
}

NSInteger FSGetDefaultInteger(NSString *key, NSInteger defaultValue)
{
    id object = _defaultForKey(key);
    if (!object)
        return defaultValue;
    
    NSNumber *number = (NSNumber *)object;
    NSInteger result = [number integerValue];
    return result;
}

BOOL FSGetDefaultBOOL(NSString *key, BOOL defaultValue)
{
    id object = _defaultForKey(key);
    if (!object)
        return defaultValue;
    
    NSNumber *number = (NSNumber *)object;
    BOOL result = [number boolValue];
    return result;
}

double FSGetDefaultDouble(NSString *key, double defaultValue)
{
    id object = _defaultForKey(key);
    if (!object)
        return defaultValue;
    
    NSNumber *number = (NSNumber *)object;
    return [number doubleValue];
}

NSString *FSGetDefaultString(NSString *key, NSString *defaultValue)
{
    id object = _defaultForKey(key);
    
    return object ? object : defaultValue;
}

void FSSetDefault(NSString *key, id value)
{
    // Setting defaults only set the standard user defaults.
    FSDefaultsFile *defaultsFile = [FSDefaultsFile sharedInstance];
    if (value == nil) {
        [defaultsFile removeObjectForKey:key];
    } else {
        [defaultsFile setObject:value forKey:key];
    }
}
