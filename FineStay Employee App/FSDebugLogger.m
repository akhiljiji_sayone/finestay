//
//  FSDebugLogger.m
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import "FSDebugLogger.h"
#import "FSDefines.h"
#import "FSDefaults.h"

NSString *_currentDateForLog()
{
    static NSDateFormatter *_formatter = nil;
    static dispatch_once_t onceDateToken;
    dispatch_once(&onceDateToken, ^{
        _formatter = [[NSDateFormatter alloc] init];
        [_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [_formatter setTimeZone:[NSTimeZone systemTimeZone]];
    });
    
    return [_formatter stringFromDate:[NSDate date]];
    
}

@interface FSDebugLogger ()

@property (nonatomic, strong)NSFileHandle *logFile;

@end

@implementation FSDebugLogger

static BOOL _loggerEnabled = NO;
static BOOL _writeToFileEnabled = YES;
static BOOL _enableConsoleLogging = NO;

+ (void)initialize
{
    _loggerEnabled = FSGetDefaultBOOL(FSLoggingEnabledKey, NO);
    _writeToFileEnabled = FSGetDefaultBOOL(FSLoggingWriteToFileEnabledKey, YES);
    NSLog(@"LOGGING ENABLED - %i. Will write to file - %i",_loggerEnabled, _writeToFileEnabled);
}

+ (FSDebugLogger *)sharedLogger
{
    if (!_loggerEnabled)
        return nil;
    
    static FSDebugLogger *_sharedLogger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedLogger = [[FSDebugLogger alloc] init];
    });
    
    return _sharedLogger;
}

+ (BOOL)isLoggingEnabled
{
    return _loggerEnabled;
}

+(void)enableLogger
{
    _loggerEnabled = YES;
    _enableConsoleLogging = YES;
    FSSetDefault(FSLoggingEnabledKey, [NSNumber numberWithInt:1]);
    FSSetDefault(FSLoggingConsoleLoggingEnabledKey, [NSNumber numberWithInt:1]);
    NSLog(@"FSDebugLogger: Enabled Logger");
}

+(void)enableConsoleLogging
{
    _enableConsoleLogging = YES;
    FSSetDefault(FSLoggingConsoleLoggingEnabledKey, [NSNumber numberWithInt:1]);
}

+(void)disableLogger
{
    _loggerEnabled = NO;
    FSSetDefault(FSLoggingEnabledKey, [NSNumber numberWithInt:0]);
    NSLog(@"FSDebugLogger: Disabled Logger");
}

+(void)enableWriteToFile
{
    _writeToFileEnabled = YES;
    FSSetDefault(FSLoggingWriteToFileEnabledKey, [NSNumber numberWithInt:1]);
    NSLog(@"FSDebugLogger: Write to file Enabled");
}

+(void)disableWriteToFile
{
    _writeToFileEnabled = NO;
    FSSetDefault(FSLoggingWriteToFileEnabledKey, [NSNumber numberWithInt:0]);
    NSLog(@"FSDebugLogger: Write to file Disabled");
    
}

NSString *logFilePath() {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"application.log"];
    
    return filePath;
}

NSString *uploadFilePath() {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"pendingUploads"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory;
    if (![fileManager fileExistsAtPath:filePath isDirectory:&isDirectory]){
        NSError *error = nil;
        [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:[NSDictionary dictionaryWithObject:@"NSFileProtectionNone" forKey:@"NSFileProtectionKey"] error:&error];
        if (error)
            NSLog(@"Unable to create upload path. Error %@",error);
        else
            NSLog(@"Created upload directory");
    }
    
    return filePath;
}

- (void)_setupLogFileHandle:(BOOL)removeExisting
{
    NSString *filePath = logFilePath();
    NSLog(@"Log file path = %@",filePath);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        if (removeExisting) {
            NSError *error = nil;
            [fileManager removeItemAtPath:filePath error:&error];
            if (error)
                NSLog(@"**** ERROR **** Unable to remove existing log file. Error : %@",error);
            else
                [fileManager createFileAtPath:filePath contents:nil attributes:[NSDictionary dictionaryWithObject:@"NSFileProtectionNone" forKey:@"NSFileProtectionKey"]];
        }
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:[NSDictionary dictionaryWithObject:@"NSFileProtectionNone" forKey:@"NSFileProtectionKey"]];
    }
    _logFile = [NSFileHandle fileHandleForWritingAtPath:filePath];
    [_logFile seekToEndOfFile];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self _setupLogFileHandle:NO];
        
        [self classInfo:[self class] log:@">>>>> Logging init %p started at %@ ",self, _currentDateForLog()];
    }
    
    return self;
}

- (void)dealloc
{
    [_logFile closeFile];
}

- (void)classInfo:classInfo log:(NSString *)format, ... {
    va_list ap;
    va_start(ap, format);
    
    NSString *message = [[NSString alloc] initWithFormat:format arguments:ap];
    if (_enableConsoleLogging)
        NSLog(@"%@: %@",classInfo,message);
    if (!_writeToFileEnabled)
        return;
    [_logFile writeData:[[NSString stringWithFormat:@"%@ %@:%@\n",_currentDateForLog(),classInfo, message] dataUsingEncoding:NSUTF8StringEncoding]];
    [_logFile synchronizeFile];
}

- (void)clearLogs
{
    if (!_writeToFileEnabled) {
        NSLog(@" - File logging is disabled. Nothing to clear -");
        return;
    }
    
    NSLog(@"FSDebugLogger: Cleared Logs");
    [self _setupLogFileHandle:YES];
    [self classInfo:[self class] log:@">>>>> clearLogs Logging %p started at %@ ",self, _currentDateForLog()];
}

- (void)dumpLogs
{
    if (!_writeToFileEnabled) {
        NSLog(@" - File logging is disabled. Nothing to dump -");
        return;
    }
    
    
    NSLog(@">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> NATIVE CLIENT LOGS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    NSError *error = nil;
    NSString *log = [NSString stringWithContentsOfFile:logFilePath() encoding:NSUTF8StringEncoding error:&error];
    if (error)
        NSLog(@"Error : %@",error);
    else
        NSLog(@"%@",log);
    NSLog(@"-----------------------------------------------------------------------------------------");
}

- (NSString *)logString
{
    return [NSString stringWithContentsOfFile:logFilePath() encoding:NSUTF8StringEncoding error:nil];
}

@end
