//
//  FSUser.m
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import "FSUser.h"

#import "FSDefaults.h"
#import "FSDefines.h"
#import "FSExtras.h"

#define FSUserEmailKey @"FSUserEmail"
#define FSFirstNameKey @"FSFirstName"
#define FSLastNameKey @"FSLastName"
#define FSGenderKey @"FSGender"
#define FSAddressKey @"FSAddress"
#define FSCheckinsKey @"FSCheckins"
#define FSDateOfBirthKey @"FSDateOfBirth"
#define FSPhoneNumbereKey @"FSPhoneNumber"
#define FSPUserIdKey @"FSPUserId"


@implementation FSUser
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        _address = SafeNonNULLValue([dictionary objectForKey:@"address"]);
        _gender = SafeNonNULLValue([dictionary objectForKey:@"gender"]);
        _checkins = SafeNonNULLValue([dictionary objectForKey:@"checkins"]);
        _dateofBirth = SafeNonNULLValue([dictionary objectForKey:@"dob"]);
        _userId = SafeNonNULLValue([dictionary objectForKey:@"id"]);
        _email = SafeNonNULLValue([dictionary objectForKey:@"email"]) ;
        _firstName = SafeNonNULLValue([dictionary objectForKey:@"fname"]);
        _lastName = SafeNonNULLValue([dictionary objectForKey:@"lname"]);
        _phoneNumber = SafeNonNULLValue([dictionary objectForKey:@"phone"]);
        _imageUrlString = SafeNonNULLValue([dictionary objectForKey:@"image_url"]);
        
        [self _updateUserDetailsInDefaults];
    }
    
    return self;
}

- (void)_updateUserDetailsInDefaults
{
    if (_address)
        FSSetDefault(FSAddressKey, _address);
    if (_gender)
        FSSetDefault(FSGenderKey, _gender);
    if (_checkins)
        FSSetDefault(FSCheckinsKey, _checkins);
    if (_dateofBirth)
        FSSetDefault(FSDateOfBirthKey, _dateofBirth);
    if (_userId)
        FSSetDefault(FSPUserIdKey, _userId);
    if (_email)
        FSSetDefault(FSUserEmailKey, _email);
    if (_firstName)
        FSSetDefault(FSFirstNameKey, _firstName);
    if (_lastName)
        FSSetDefault(FSLastNameKey, _lastName);
    if (_phoneNumber)
        FSSetDefault(FSPhoneNumbereKey, _phoneNumber);
    if (_imageUrlString)
        FSSetDefault(FSUserImageKey, _imageUrlString);
}

+ (FSUser *)getLoggedInUserFromDefaults
{
    FSUser *user = [[FSUser alloc]init];
    user.firstName = FSGetDefaultString(FSFirstNameKey, nil);
    user.lastName = FSGetDefaultString(FSLastNameKey, nil);
    user.gender = FSGetDefaultString(FSGenderKey, nil);
    user.address = FSGetDefaultString(FSAddressKey, nil);
    user.dateofBirth = FSGetDefaultString(FSDateOfBirthKey, nil);
    user.userId = FSGetDefaultString(FSPUserIdKey, nil);
    user.phoneNumber = FSGetDefaultString(FSPhoneNumbereKey, nil);
    user.checkins = FSGetDefault(FSCheckinsKey);
    user.email = FSGetDefaultString(FSUserEmailKey, nil);
    user.imageUrlString = FSGetDefaultString(FSUserImageKey, nil);
    NSString *token = FSGetDefault(FSCSRFToken);
    if (token.length > 0)
        user.userStatus = UserStatusLoggedIn;
    else
        user.userStatus = UserStatusNotLoggedIn;
    
    return user;
}

+ (void)resetUserDefaults
{
    FSSetDefault(FSAddressKey, nil);
    FSSetDefault(FSGenderKey, nil);
    FSSetDefault(FSCheckinsKey, nil);
    FSSetDefault(FSDateOfBirthKey, nil);
    FSSetDefault(FSPUserIdKey, nil);
    FSSetDefault(FSUserEmailKey, nil);
    FSSetDefault(FSFirstNameKey, nil);
    FSSetDefault(FSLastNameKey, nil);
    FSSetDefault(FSPhoneNumbereKey, nil);
    FSSetDefault(FSCSRFToken, nil);
    FSSetDefault(FSUserImageKey, nil);
}

@end
