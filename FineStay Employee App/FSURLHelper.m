//
//  FSURLHelper.m
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import "FSURLHelper.h"

@implementation FSURLHelper

+(FSURLHelper *)sharedURLHelper
{
    static FSURLHelper *__sharedURLHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedURLHelper = [[FSURLHelper alloc] init];
    });
    
    return __sharedURLHelper;
}

- (NSString *)apiEndpoint
{
    if (!_apiEndpoint) {
        _apiEndpoint = [[NSUserDefaults standardUserDefaults] stringForKey:@"api_endpoint_preference"];
        if (!_apiEndpoint)
            self.apiEndpoint = @"https://api-s.FScoapp.in";
    }
    
    return _apiEndpoint;
}

- (NSString *)storesURLString
{
    if (!_storesURLString) {
        _storesURLString = [NSString stringWithFormat:@"%@/api/v4/stores/?loc=",self.apiEndpoint];
    }
    
    return _storesURLString;
}

- (NSString *)storeDetailsURLString
{
    if (!_storeDetailsURLString) {
        _storeDetailsURLString = [NSString stringWithFormat:@"%@/api/v1/storedetail/?store_ids=",self.apiEndpoint];
    }
    
    return _storeDetailsURLString;
}

- (NSString *)citiesURLString
{
    if (!_citiesURLString) {
        _citiesURLString = [NSString stringWithFormat:@"%@/api/v1/stores/geo/cities/?loc=",self.apiEndpoint];
    }
    
    return _citiesURLString;
}

- (NSString *)forgotURLString
{
    if (!_forgotURLString) {
        _forgotURLString = [NSString stringWithFormat:@"%@/api/v1/reset-password/",self.apiEndpoint];
    }
    
    return _forgotURLString;
}

- (NSString *)dealURLString
{
    if (!_dealURLString) {
        _dealURLString = [NSString stringWithFormat:@"%@/api/v5/deals/?loc=",self.apiEndpoint];
    }
    
    return _dealURLString;
}

- (NSString *)dealFilterURLString
{
    if (!_dealFilterURLString) {
        _dealFilterURLString = [NSString stringWithFormat:@"%@/api/v5/deals/?loc=",self.apiEndpoint];
    }
    
    return _dealFilterURLString;
}

- (NSString *)registerURLString
{
    if (!_registerURLString) {
        _registerURLString = [NSString stringWithFormat:@"%@/api/v2/register/",self.apiEndpoint];
    }
    
    return _registerURLString;
}

- (NSString *)loginURLString
{
    if (!_loginURLString) {
        _loginURLString = [NSString stringWithFormat:@"%@/api/v1/login/",self.apiEndpoint];
    }
    
    return _loginURLString;
}

- (NSString *)checkMailURLString
{
    if (!_checkMailURLString) {
        _checkMailURLString = [NSString stringWithFormat:@"%@/api/v1/check-email/",self.apiEndpoint];
    }
    
    return _checkMailURLString;
}

- (NSString *)userGetInfoURLString
{
    if (!_userGetInfoURLString) {
        _userGetInfoURLString = [NSString stringWithFormat:@"%@/api/v2/account/",self.apiEndpoint];
    }
    
    return _userGetInfoURLString;
}

- (NSString *)facebookLoginURLString
{
    if (!_facebookLoginURLString) {
        _facebookLoginURLString = [NSString stringWithFormat:@"%@/api/v1/login/facebook/",self.apiEndpoint];
    }
    
    return _facebookLoginURLString;
}

- (NSString *)googleLoginURLString
{
    if (!_googleLoginURLString) {
        _googleLoginURLString = [NSString stringWithFormat:@"%@/api/v1/login/google/",self.apiEndpoint];
    }
    
    return _googleLoginURLString;
}

- (NSString *)profileImageUpdateURLString
{
    if (!_profileImageUpdateURLString) {
        _profileImageUpdateURLString = [NSString stringWithFormat:@"%@/api/v2/account/update-profile-image/",self.apiEndpoint];
    }
    
    return _profileImageUpdateURLString;
}

- (NSString *)feedbackURLString
{
    if (!_feedbackURLString) {
        _feedbackURLString = [NSString stringWithFormat:@"%@/api/v1/feedback/",self.apiEndpoint];
    }
    
    return _feedbackURLString;
}


@end
