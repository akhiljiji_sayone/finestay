//
//  FSDebugLogger.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import <Foundation/Foundation.h>

#   define DLog(fmt, ...) [[FSDebugLogger sharedLogger] classInfo:[self class] log:fmt, ##__VA_ARGS__]

@interface FSDebugLogger : NSObject

+(FSDebugLogger *)sharedLogger;
+(void)enableLogger;
+(void)disableLogger;
+ (BOOL)isLoggingEnabled;
+(void)enableWriteToFile;
+(void)enableConsoleLogging;
+(void)disableWriteToFile;

- (void)classInfo:className log:(NSString *)format, ...;
- (void)clearLogs;
- (void)dumpLogs;


@end
