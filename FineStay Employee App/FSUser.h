//
//  FSUser.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, UserStatus) {
    UserStatusNotLoggedIn = 0,
    UserStatusLoggedIn = 1,
};


@interface FSUser : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *header;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSArray *checkins;
@property (nonatomic, strong) NSString *dateofBirth;
@property (nonatomic, strong) NSString *imageUrlString;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *provider;
@property (nonatomic, strong) NSString * userId;
@property (nonatomic) UserStatus userStatus;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
+ (void)resetUserDefaults;
+ (FSUser *)getLoggedInUserFromDefaults;


@end
