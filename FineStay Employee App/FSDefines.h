//
//  FSDefines.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#define FSLoggingEnabledKey @"FSLoggingEnabled"
#define FSLoggingWriteToFileEnabledKey @"FSLoggingWriteToFileEnabled"
#define FSLoggingConsoleLoggingEnabledKey @"FSLoggingConsoleLoggingEnabled"
#define FSUserImageKey @"FSUserImage"
#define FSCSRFToken @"csrftoken"
#define FSAPIConnectionTimeOutKey @"FSAPIConnectionTimeOut"
