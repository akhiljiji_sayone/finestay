//
//  FSExtras.m
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import "FSExtras.h"
#include <libkern/OSAtomic.h>
#import <UIKit/UIApplication.h>

static int32_t __inflightCounter = 0;

void IncrementInFlightRequestForNetworkActivityIndicator()
{
    int32_t count = OSAtomicIncrement32(&__inflightCounter);
    if (count > 0)
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

void DecrementInFlightRequestForNetworkActivityIndicator()
{
    int32_t count = OSAtomicDecrement32(&__inflightCounter);
    if (count <= 0)
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

NSString *FSDeviceIdentifier()
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

BOOL EmailIsValid(NSString *emailAddress)
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailAddress];
}

