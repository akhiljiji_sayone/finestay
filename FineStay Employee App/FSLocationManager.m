//
//  FSLocationManager.m
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import "FSLocationManager.h"

@implementation FSLocationManager

+(FSLocationManager *)sharedLocationManager
{
    static FSLocationManager *__sharedLocationManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedLocationManager = [[FSLocationManager alloc] init];
    });
    
    return __sharedLocationManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [_locationManager requestWhenInUseAuthorization];
    }
    
    return self;
}

- (void)updateLocationWithCompletionHandler:(FSLocationManagerHandler)completion
{
    self.completionBlock = completion;
    [self.locationManager startUpdatingLocation];
}

#pragma mark - Location manager delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    [manager stopUpdatingLocation];
    if (_completionBlock) {
        _completionBlock(currentLocation, nil, NO);
        self.completionBlock = nil;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    _location = nil;
    if(error.domain == kCLErrorDomain && error.code == kCLErrorDenied) {
        [self locationUpdatingFailedWithError:error locationServiceDisabled:YES];
        return;
    }
}

#pragma mark - Private helper methods

- (void)locationUpdatingFailedWithError:(NSError *)error locationServiceDisabled:(BOOL)locationServiceDisabled
{
    [self.locationManager stopUpdatingLocation];
    if (_completionBlock) {
        _completionBlock(nil, error, locationServiceDisabled);
    }
}




@end
