//
//  FSURLHelper.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSURLHelper : NSObject

+(FSURLHelper *)sharedURLHelper;

@property (nonatomic, strong)NSString *apiEndpoint;

@property (nonatomic, strong)NSString *storesURLString;

@property (nonatomic, strong)NSString *storeDetailsURLString;

@property (nonatomic, strong)NSString *citiesURLString;

@property (nonatomic, strong)NSString *forgotURLString;

@property (nonatomic, strong)NSString *dealURLString;

@property (nonatomic, strong)NSString *dealFilterURLString;

@property (nonatomic, strong)NSString *registerURLString;

@property (nonatomic, strong)NSString *loginURLString;

@property (nonatomic, strong)NSString *checkMailURLString;

@property (nonatomic, strong)NSString *userGetInfoURLString;

@property (nonatomic, strong)NSString *facebookLoginURLString;

@property (nonatomic, strong)NSString *googleLoginURLString;

@property (nonatomic, strong)NSString *profileImageUpdateURLString;

@property (nonatomic, strong)NSString *feedbackURLString;


@end
