//
//  FSLocationManager.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

typedef void (^FSLocationManagerHandler)(CLLocation *location, NSError *error, BOOL locationServiceDisabled);

@interface FSLocationManager : NSObject <CLLocationManagerDelegate>

+ (FSLocationManager *)sharedLocationManager;

@property (nonatomic, readonly) CLLocation *location;

@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic, copy) FSLocationManagerHandler completionBlock;

- (void)updateLocationWithCompletionHandler:(FSLocationManagerHandler)completion;


@end
