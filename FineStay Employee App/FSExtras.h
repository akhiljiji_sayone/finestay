//
//  FSExtras.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "FSLocationManager.h"


NS_INLINE id SafeNonNULLValue(id object) {
    return object == (id)[NSNull null] ? nil : object;
}

NS_INLINE UITableViewCell *DequeOrCreateCellInTableView(NSString *identifier, UITableView *tableView)
{
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

extern void IncrementInFlightRequestForNetworkActivityIndicator();

extern void DecrementInFlightRequestForNetworkActivityIndicator();

extern NSString *FSDeviceIdentifier();

extern BOOL EmailIsValid(NSString *emailAddress);

NS_INLINE NSString *DistanceFromCurrentLocation(NSDictionary *geoLocation)
{
    CLLocation *currentLocation = [[[FSLocationManager sharedLocationManager] locationManager]location];
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:[[geoLocation objectForKey:@"lat"] doubleValue] longitude:[[geoLocation objectForKey:@"lng"] doubleValue]];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude];
    CLLocationDistance distance = [locA distanceFromLocation:locB]/ 1000;
    
    return [NSString stringWithFormat:@"%.2f km", distance];
}

NS_INLINE NSString *GetImageURLString(NSArray *images, NSString *imageSize)
{
    NSString *deviceResolution =  [NSString stringWithFormat:@"%.fx",[UIScreen mainScreen].scale];
    if (images.count == 2) {
        return [NSString stringWithFormat:@"%@%@%@%@",[images objectAtIndex:0],imageSize,deviceResolution,[images objectAtIndex:1]];
    }
    
    return nil;
}
