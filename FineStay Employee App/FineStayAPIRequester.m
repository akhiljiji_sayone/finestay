//
//  FineStayAPIRequester.m
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import "FineStayAPIRequester.h"
#import "FSUser.h"
#import "FSExtras.h"
#import "FSDebugLogger.h"
#import "FSDefaults.h"
#import "FSDefines.h"

NS_INLINE FSStatus _ErrorFSStatusFromHTTPStatusCode(NSInteger httpStatusCode) {
    
    return FSStatusRequestFailed;
}

NS_INLINE BOOL _isRequestSuccessWithStatusCode(NSInteger httpStatusCode) {
    return (httpStatusCode == 200 || httpStatusCode == 202 || httpStatusCode == 204);
}

@interface FineStayAPIRequester ()
{
    CFAbsoluteTime _requestStartTime;
}

@property (nonatomic,retain)NSURLConnection *connection;
@property (nonatomic, retain, readwrite)NSMutableData *serverResponseData; //IntermediateResponse
@property (nonatomic, retain) NSThread *startThread;
@property (nonatomic, strong) NSDictionary *responseHeaders;
@property (nonatomic, strong) NSString *postPayload;

@end


@implementation FineStayAPIRequester

- (id)initWithUser:(FSUser *)user finishedHandler:(FSAPIRequesterFinishedHandler)handler
{
    self = [super init];
    if (self) {
        self.finishedHandler = handler;
        self.user = user;
    }
    
    return self;
}

- (void)dealloc
{
    [self _cleanup];
}

- (void)_cleanup
{
    [_connection cancel];
    _connection = nil;
    _serverResponseData = nil;
    _startThread = nil;
}

- (void)cancel
{
    NSThread *executionThread = self.startThread;
    if (executionThread) {
        if (executionThread != [NSThread currentThread]) {
            [self performSelector:@selector(cancel) onThread:executionThread withObject:nil waitUntilDone:NO];
            return;
        }
    }
    
    DecrementInFlightRequestForNetworkActivityIndicator();
    [self.connection cancel];
    [self _cleanup];
    
    if (_finishedHandler) {
        _finishedHandler(FSStatusRequestCancelled, nil, nil);
    }
}

- (NSString *)httpMethod
{
    return _httpMethod ? _httpMethod : kHTTPPOSTMethod;
}

- (NSString *)contentType
{
    return _contentType ? _contentType : @"application/json";
}

- (NSURLRequest *)urlRequest
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    NSString *httpMethod = self.httpMethod;
    [request setHTTPMethod:httpMethod];
    [request setValue:self.contentType forHTTPHeaderField:@"Content-Type"];
    [request setTimeoutInterval:_timeout > 0 ? _timeout : FSGetDefaultInteger(FSAPIConnectionTimeOutKey, 15)];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
    
#if (TARGET_IPHONE_SIMULATOR)
    [request setValue:@"iPhone" forHTTPHeaderField:@"x-client-device"];
    [request setValue:@"60034ED4-D4C6-4317-B2F3-79EFD6311266" forHTTPHeaderField:@"x-device-id"];
#else
    UIDevice *device = [UIDevice currentDevice];
    [request setValue:[device model] forHTTPHeaderField:@"x-client-device"];
    [request setValue:FSDeviceIdentifier() forHTTPHeaderField:@"x-device-id"];
#endif
    
    NSData *postData = self.postData;
    [request setURL:self.url];
    
    if (postData)
        _postPayload = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
    
    [request setHTTPBody:postData];
    [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    
    return request;
}

- (void)start
{
    NSThread *currentThread = [NSThread currentThread];
    if (self.startThread != currentThread)
        self.startThread = currentThread;
    
    
    NSURLConnection *connection = self.connection;
    if (connection)
        [connection cancel];
    
    connection = [[NSURLConnection alloc] initWithRequest:[self urlRequest] delegate:self startImmediately:NO];
    self.connection = connection;
    
    IncrementInFlightRequestForNetworkActivityIndicator();
    [connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [connection start];
    _requestStartTime = CFAbsoluteTimeGetCurrent();
}

- (id)startSynchronousReturnStatusCode:(FSStatus *)status error:(NSError **)error
{
    NSHTTPURLResponse* response = nil;
    NSURLRequest *request = [self urlRequest];
    CFAbsoluteTime startime = CFAbsoluteTimeGetCurrent();
    IncrementInFlightRequestForNetworkActivityIndicator();
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:error];
    _responseHeaders = [response respondsToSelector:@selector(allHeaderFields)] ? [(id)response allHeaderFields] : nil;
    DecrementInFlightRequestForNetworkActivityIndicator();
    double seconds = CFAbsoluteTimeGetCurrent() - startime;
    NSString *operationName = [self operationName];
    if (!operationName)
        operationName = @"";
    if (_postPayload.length > 2){
        DLog(@"%@ startSynchronousComplete: %@", operationName, _postPayload);
        DLog(@"%@ startSynchronousComplete: %@ %@ HTTP Status : %i sec: %f Error:%@",operationName,self.httpMethod,request.URL,response.statusCode,seconds, *error);
    } else {
        DLog(@"%@ startSynchronousComplete: %@ %@ HTTP Status : %i sec: %f Error:%@",operationName, self.httpMethod,request.URL,response.statusCode,seconds, *error);
    }
    
    
    NSInteger statusCode = FSStatusSuccess;
    NSDictionary *errorUserInfo = nil;
    if (!response && responseData && responseData.length > 0) {
        NSError *error;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        if ([responseDict isKindOfClass:[NSDictionary class]]) {
            statusCode = [[responseDict objectForKey:@"status"] intValue];
            errorUserInfo = responseDict;
        }
    } else {
        statusCode = response.statusCode;
    }
    
    _httpStatusCode = statusCode;
    _rawHttpStatusCode = statusCode;
    if (_isRequestSuccessWithStatusCode(statusCode)) {
        *status = FSStatusSuccess;
        return responseData.length > 0 ? [self handleResponseData:responseData] : nil;
    } else {
        *error = [NSError errorWithDomain:@"FSHTTPError" code:statusCode userInfo:errorUserInfo];
        *status = _ErrorFSStatusFromHTTPStatusCode(statusCode);
        return nil;
    }
}


// Should FS done by the subclasses.
- (id)handleResponseData:(NSData *)responseData
{
    // Nothing to do here. subclass need to implement this.
    [[NSException exceptionWithName:@"Method undefined" reason:@"Subclass needs to implement handleResponseData:" userInfo:nil] raise];
    
    return nil;
}

- (NSURL *)url
{
    // Nothing to do here. subclass need to implement this.
    [[NSException exceptionWithName:@"Method undefined" reason:@"Subclass needs to implement url" userInfo:nil] raise];
    
    return nil;
}

#pragma NSURLConnection delegate messages
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    //    DLog(@"connection:%@didReceiveData:",connection);
    
    NSMutableData *responseData = self.serverResponseData;
    if (responseData == nil) {
        responseData = [[NSMutableData alloc] initWithData:data];
        self.serverResponseData = responseData;
    } else {
        [responseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DecrementInFlightRequestForNetworkActivityIndicator();
    DLog(@"connection:%@didFailWithError: %@",connection,error);
    if (_finishedHandler) {
        _finishedHandler(FSStatusRequestFailed, nil, error);
    }
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    _httpStatusCode = [httpResponse statusCode];
    _rawHttpStatusCode = _httpStatusCode;
    _responseHeaders = [httpResponse respondsToSelector:@selector(allHeaderFields)] ? [(id)httpResponse allHeaderFields] : nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DecrementInFlightRequestForNetworkActivityIndicator();
    if (self.connection != connection)
        return;
    
    float seconds = (CFAbsoluteTimeGetCurrent() - _requestStartTime);
    NSString *operationName = [self operationName];
    if (!operationName)
        operationName = @"";
    if (_postPayload.length > 2){
        DLog(@"%@ connectionDidFinishLoading:%@",operationName, _postPayload);
        DLog(@"%@ connectionDidFinishLoading:%@ %@ sec: %f HTTP Status : %i",operationName, self.httpMethod, connection.originalRequest.URL,seconds, _httpStatusCode);
    } else {
        DLog(@"%@ connectionDidFinishLoading:%@ %@ sec: %f HTTP Status : %i",operationName, self.httpMethod, connection.originalRequest.URL,seconds, _httpStatusCode);
    }
    
    _connection = nil;
    if (_finishedHandler) {
        if (_isRequestSuccessWithStatusCode(_httpStatusCode)) {
            id object = nil;
            if (_serverResponseData.length > 0)
                object = [self handleResponseData:_serverResponseData];
            _finishedHandler(FSStatusSuccess, object, nil);
        } else {
            NSError *error = [NSError errorWithDomain:@"FSHTTPError" code:_httpStatusCode userInfo:nil];
            id errorResponse = nil;
            if (_serverResponseData.length > 0) {
                NSError *JSONerror = nil;
                errorResponse = [NSJSONSerialization JSONObjectWithData:_serverResponseData options:kNilOptions error:&JSONerror];
            }
            
            
            _finishedHandler(_ErrorFSStatusFromHTTPStatusCode(_httpStatusCode), errorResponse, error);
        }
    }
}

- (NSString *)operationName
{
    return nil;
}

#pragma mark NSURLConnection delegate for not caching
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}



@end
