//
//  FineStayAPIRequester.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FSStatus) {
    FSStatusSuccess = 0,
    
    // Request Errors
    FSStatusRequestCancelled = 1,
    FSStatusRequestFailed = 2,
};

static NSString * const kHTTPGETMethod = @"GET";
static NSString * const kHTTPPOSTMethod = @"POST";
static NSString * const kHTTPPUTMethod = @"PUT";
static NSString * const kHTTPDELETEMethod = @"DELETE";

typedef void (^FSAPIRequesterFinishedHandler)(FSStatus status, id responseObject, NSError* error);

@class FSUser;

@interface FineStayAPIRequester : NSObject

@property (nonatomic, strong)NSData *postData;
@property (nonatomic, strong)NSURL *url;           // Subclasses will have to override this with the URL to hit.
@property (nonatomic, strong)NSString *httpMethod; // This is defaulted to @"POST"
@property (nonatomic, strong)NSString *contentType; // Default set to application/json
@property (nonatomic)int timeout;
@property (nonatomic, readonly)NSDictionary *responseHeaders;

@property (nonatomic, readonly)NSInteger httpStatusCode;
@property (nonatomic, readonly)NSInteger rawHttpStatusCode;
@property (nonatomic, copy)FSAPIRequesterFinishedHandler finishedHandler;
@property (nonatomic, strong)FSUser *user;

- (id)initWithUser:(FSUser *)user finishedHandler:(FSAPIRequesterFinishedHandler)handler;

// This generates the url request for the requester. This is already taken care for you.
// Override this if and only if you need to change the payload from the default JSON Payload.
- (NSURLRequest *)urlRequest;

// Async request.
- (void)start;
- (void)cancel;

// Sync request
- (id)startSynchronousReturnStatusCode:(FSStatus *)status error:(NSError **)error;

// Subclasses can handle the response data here. This will then FS handed back in the finished Handler as the processed object
- (id)handleResponseData:(NSData *)responseData;

@end
