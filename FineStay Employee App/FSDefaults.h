//
//  FSDefaults.h
//  FineStay Employee App
//
//  Created by SayOne Technologies on 17/07/17.
//  Copyright © 2017 SayOne. All rights reserved.
//

#import <Foundation/Foundation.h>
NSInteger FSGetDefaultInteger(NSString *key, NSInteger defaultValue);

BOOL FSGetDefaultBOOL(NSString *key, BOOL defaultValue);

double FSGetDefaultDouble(NSString *key, double defaultValue);

NSString *FSGetDefaultString(NSString *key, NSString *defaultValue);

void FSSetDefault(NSString *key, id value);
id FSGetDefault(NSString *key);

